function getData(params, startTime, endTime, callback, fail) {
    if (startTime !== null) {
        params.start_time = startTime;
    }
    if (endTime !== null) {
        params.end_time = endTime;
    }
    $.get({
        url: url(),
        data: params,
        contentType: 'application/json',
        dataType: 'json'
    }).done(function (responseData, textStatus, jqXHR) {
        if (callback !== undefined) {
            callback(responseData);
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (fail !== undefined) {
            fail(errorThrown);
        }
    });
}

function onload() {
    initProgressBar(1);
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
    $('#main').hide()
    $('#mainSpinner').show();
    getSites(true, function (resp) {
        advanceProgressBar();
        sites = resp;
        loadSummaryPage();
        loadSiteDashboards();
        loadPlanktonPortal();
        $('#main').show()
        $('#mainSpinner').hide();
        showSummaryPage();
        //loadAnalytics();
    });
}

function initProgressBar(total) {
    $('#progress').width(0);
    $('#progress').attr('data-total', total);
    $('#progress').attr('data-current', 0);
}

function advanceProgressBar() {
    var total = parseInt($('#progress').attr('data-total'));
    var current = parseInt($('#progress').attr('data-current')) + 1;
    if (total === current) {
        return;
    }
    $('#progress').width(parseInt((current / (total + 1)) * 100) + '%');
    $('#progress').attr('data-current', current);
    if (total === current) {
        $('#mainSpinner').hide()
        $('#main').show();
        $.each(charts, function () {
            this.redraw();
        })
    }
}

function dt(date) {
    if (date === null) {
        date = new Date();
    }
    return moment.utc(date).format('Y-MM-DDTHH:mm:ss') + 'Z';
}

function url(path) {
    var url = window.location.pathname.split('/').slice(0, -2).join('/') + '/';
    if (path !== undefined) {
        url = url + path;
    }
    return url;
}
