var inputs = [],
    prodAreas = {};

function updateNavBarPlankton() {
    $('#navbar-list').append($('<li>').attr('class', 'nav-item')
                                    .append($('<a>').attr('class', 'nav-link').attr('onclick', 'showPlanktonPortal()')
                                                .attr('href', '#').append('PlanktonPortal')))
}

function loadPlanktonPortal() {
    'use strict';
    $('#main').append($('<div>').attr('id', 'plankton-portal').attr('class', 'container').attr('style', 'display:none'));
    $('#plankton-portal').load('plankton.html', null, function () {
        var maxDate = new Date();
        maxDate.setHours(23, 59, 59, 999);   // end of the current day
        rome($('#datetime')[0], { max: maxDate, initialValue: new Date() });
        $('.plankton-box').keydown(function (event) {
            var inputs = $('.plankton-box').filter(':visible'),
                current = inputs.index(this),
                target;
            if (event.keyCode == 37) {  // left arrow
                target = mod(current - 1, inputs.length);
            } else if (event.keyCode == 39) {   // right arrow
                target = mod(current + 1, inputs.length);
            } else {
                return;
            }
            inputs[target].focus();
        });
        $('#planktonModal').on('shown.bs.modal', function () { $('#numCells').select(); });
        updateNavBarPlankton();
    });
}

function mod(n, m) {
  // ensure we're not taking the modulo of a negative number... because javascript
  return ((n % m) + m) % m;
}

function showPlanktonPortal() {
    $('#main').children().hide();
    $('#plankton-portal').show();
    if (Object.keys(sites).length === 1) {
        var siteName = Object.keys(sites)[0],
            locations = sites[siteName].sampleLocations,
            prodArea = sites[siteName].prodArea;
        $('#prodAreaLabel').text(prodArea);
        $('#siteNameLabel').text(siteName);
        $('#prodAreaSelectContainer').hide();
        $('#prodAreaLabel').show();
        $('#siteNameSelectContainer').hide();
        $('#siteNameLabel').show();
        selectSite(siteName);
    } else {
        var siteNames = Object.keys(sites).sort();
        $('#prodAreaSelectContainer').show();
        $('#prodAreaLabel').hide();
        $('#siteNameSelectContainer').show();
        $('#siteNameLabel').hide();
        for (var i = 0; i < siteNames.length; i++) {
            var siteName = siteNames[i],
                prodArea = sites[siteName].prodArea;
            if (Object.keys(prodAreas).indexOf(prodArea) == -1) {
                prodAreas[prodArea] = [];
            }
            prodAreas[prodArea].push(siteName);
        }
        var pnames = Object.keys(prodAreas).sort();
        for (var j = 0; j < pnames.length; j++) {
            var name = pnames[j];
            $('#prodAreaSelect').append($('<option>').attr('value', name).append(name));
        }
    }
}

function selectProdArea(value) {
    'use strict';
    if (value.length > 0) {
        var siteNames = prodAreas[value];
        $('#siteNameSelect').empty();
        $('#locationName').empty();
        $('#measuresForm').hide();
        $('#nutrientsForm').hide();
        $('#planktonForm').hide();
        $('#siteNameSelect').append($('<option selected disabled value=""></option>'));
        $.each(prodAreas[value], function (idx, name) {
            $('#siteNameSelect').append($('<option></option>').attr('value', name).text(name));
        });
        $('#siteNameSelect').removeAttr('disabled');
    }
}

function selectSite(siteName) {
    'use strict';
    var locations = sites[siteName].locations;
    $('#locationName').empty();
    $('#measuresForm').hide();
    $('#nutrientsForm').hide();
    $('#planktonForm').hide();
    $('#locationName').append($('<option selected disabled value=""></option>'));
    $.each(locations, function (index, name) {
        $('#locationName').append($('<option></option>').attr('value', name).text(name));
    });
    $('#locationName').removeAttr('disabled');
}

function removeValid(element) {
    'use strict';
    element.parent().removeClass('has-success');
    element.parent().removeClass('has-error');
}

function showHouseForm() {
    'use strict';
    $('#measuresForm').show();
    $('#nutrientsForm').show();
    $('#planktonForm').show();
    $('#ascForm').hide();
    $.each($('.validate-input'), function (index, input) {
        removeValid($(input));
    });
    // Enable everything for House
    $.each($('.plankton-box'), function (index, input) {
        $(input).prop('disabled', false);
    });
    $('#cfm_pen_row').hide();
    $('#cfm_walkway_row').hide();
}

function showPenForm() {
    'use strict';
    $('#measuresForm').show();
    $('#nutrientsForm').show();
    $('#planktonForm').show();
    $('#ascForm').hide();
    $.each($('.validate-input'), function (index, input) {
        removeValid($(input));
    });

    $.each($('.plankton-box'), function (index, input) {
        input = $(input);
        if (input.attr('id').indexOf('20_meter') >= 0 || input.attr('id').indexOf('25_meter') >= 0 || input.attr('id').indexOf('15_meter_tow') >= 0) {
            input.prop('disabled', true);
        }
    });
    $('#cfm_pen_row').show();
    $('#cfm_walkway_row').show();
}

function showSeedForm() {
    'use strict';
    $('#measuresForm').show();
    $('#nutrientsForm').show();
    $('#planktonForm').show();
    $('#ascForm').hide();
    $.each($('.validate-input'), function (index, input) {
        removeValid($(input));
    });

    // Disable Environmental inputs for Seed areas
    $.each($('#measuresForm :input'), function (index, input) {
        $(input).prop('disabled', true);
    });
    $('#cfm_pen_row').hide();
    $('#cfm_walkway_row').hide();
}

function showASCForm() {
    'use strict';
    $('#measuresForm').hide();
    $('#nutrientsForm').hide();
    $('#planktonForm').hide();
    $('#ascForm').show();
    $.each($('.validate-input'), function (index, input) {
        removeValid($(input));
    });
}

function showForm() {
    'use strict';
    var dateTimeVal = $('#datetime').val(),
        locationVal = $('#locationName').val();
    if (dateTimeVal !== null && dateTimeVal.length > 0 &&
            locationVal !== null && locationVal.length > 0) {
        if (locationVal.indexOf('ASC') >= 0) {
            showASCForm();
        } else if (locationVal.indexOf('Pen') >= 0) {
            showPenForm();
        } else if (locationVal.indexOf('Pen') === -1 && locationVal.indexOf('House') === -1) {
            showSeedForm();
        } else {
            showHouseForm();
        }
        $('#submitButtonForm').show();
    }
}

function showPlanktonModal(button) {
    'use strict';
    $('#planktonModal').modal('show');
    $('#numSlides').val('1');
    $('#numQuadrants').val('1000');
    $('#numCells').val('');
    $('#planktonModalSubmit').unbind('click');
    $('#planktonModalSubmit').on('click', function () {
        if (!validatePlanktonSubmit()) {
            return;
        }
        var parts = $(button).attr('id').split('_'),
            depth = parseInt(parts[parts.length - 3], 10),
            nameLower = parts.splice(0, parts.length - 3).join(' '),
            name = nameLower.charAt(0).toUpperCase() + nameLower.slice(1),
            threshold = $('#' + nameLower.split(' ').join('_').replace('.', '\\.')).attr('thresholds').split(','),
            slides = parseInt($('#numSlides').val(), 10),
            quadrants = parseInt($('#numQuadrants').val(), 10),
            cells = parseInt($('#numCells').val(), 10),
            value = (cells / slides) * (1000 / quadrants),
            displayValue = value,
            area,
            volume,
            concFactor,
            colorClass;
        if (parts.indexOf('tow') >= 0) {
            area = Math.PI * Math.pow(0.15, 2);
            volume = area * depth;
            concFactor = (Math.pow(0.15, 2) * depth * Math.PI) / volume;
            value = (value / concFactor) / (volume * 1000000);
            displayValue = value * 1000000;
        }

        if (threshold.length > 0) {
            colorClass = 'threshold-ok';
            if (value >= parseInt(threshold[0], 10) && value <= parseInt(threshold[1], 10)) {
                colorClass = 'threshold-warn';
            } else if (value > parseInt(threshold[1], 10)) {
                colorClass = 'threshold-danger';
            }
            $(button).attr('class', 'btn btn-secondary plankton-box ' + colorClass);
        }
        $(button).text(Math.round(displayValue));
        $(button).prop('value', value);
        $('#planktonModal').modal('hide');
    });
}

function validateSubmit() {
    'use strict';
    var submitButton = $('#submitButton'),
        location = $('#locationName').val(),
        inputElements = null,
        errors = [];
    inputElements = $('.validate-input');
    $.each(inputElements, function (index, inputElement) {
        var err = isValid($(inputElement));
        if (err !== null) {
            errors.push(err);
        }
    });
    if (errors.length > 0) {
        $('#submitErrorMessage').html(errors.join(''));
        return false;
    }
    return true;
}

function setElementValid(element, isValid) {
    'use strict';
    removeValid(element);
    if (isValid) {
        element.parent().addClass('has-success');
    } else {
        element.parent().addClass('has-error');
    }
}

function isValid(element) {
    'use strict';
    var elementValidators = element.attr('validators'),
        error = null;
    if (elementValidators === null || elementValidators === undefined || elementValidators.length === 0 || element.is(':hidden') || element.is(':disabled')) {
        return error;
    }
    $.each(elementValidators.split(','), function (index, name) {
        if (error !== null) {
            return;
        }
        error = validators[name](element);
    });
    setElementValid(element, error === null);
    return error;
}

function validatePlanktonSubmit() {
    'use strict';
    var numSlides = $('#numSlides').val(),
        numQuadrants = $('#numQuadrants').val(),
        numCells = $('#numCells').val(),
        slidesValid = numSlides.length > 0,
        quadrantsValid = numQuadrants.length > 0,
        cellsValid = numCells.length > 0;
    if (!slidesValid) {
        $('#numSlides').parent().addClass('has-error');
    } else {
        $('#numSlides').parent().removeClass('has-error');
    }
    if (!quadrantsValid) {
        $('#numQuadrants').parent().addClass('has-error');
    } else {
        $('#numQuadrants').parent().removeClass('has-error');
    }
    if (!cellsValid) {
        $('#numCells').parent().addClass('has-error');
    } else {
        $('#numCells').parent().removeClass('has-error');
    }
    return slidesValid && quadrantsValid && cellsValid;
}

function getAllData() {
    'use strict';
    var location = $('#locationName').val(),
        values = [],
        input,
        timestamp = moment($('#datetime').val()).format('YYYY-MM-DDTHH:mm:ssZZ'),
        val;
    $.each($('.plankton-box'), function (index, input) {
        input = $(input);
        if (input.attr('id').indexOf('time') >= 0 || input.is(':hidden') || input.is(':disabled')) {
            return;
        }
        val = input.val();
        if (input.prop('tagName') === 'BUTTON') {
            val = input.prop('value');
        }
        if (val === "") {
            return;     // skip empty values
        }
        values.push({
            'qid': ['PlanktonPortal', location, input.attr('id')].join('-'),
            'ts': timestamp,
            'val': val
        });
    });
    $.each($('input:radio:checked'), function (index, input) {
        values.push({
            'qid': ['PlanktonPortal', location, input.name].join('-'),
            'ts': timestamp,
            'val': input.value
        });
    });
    return values;
}

function submitData() {
    'use strict';
    var isValid = validateSubmit(),
        data = getAllData(),
        site = $('#siteNameLabel').text();
    if (site.length === 0) {
        site = $('#siteNameSelect').val()
    }
    $('#submitModal').modal('show');
    $('#submit-modal-title').text('Processing');
    $('#submitError').hide();
    $('#submitSuccess').hide();
    $('#submitPending').show();
    if (isValid) {
        $.ajax({
            method: 'PUT',
            url: url() + '?validate=true&bname=' + site + '_PlanktonPortal',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'text'
        }).done(function (responseData, textStatus, jqXHR) {
            $('#submitPending').hide();
            $('#submitSuccess').show();
            $('#submit-modal-title').text('Submitted');
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var code = jqXHR.statusCode();
            $('#submitPending').hide();
            $('#submitError').show();
            $('#submitErrorMessage').text(jqXHR.responseJSON.message);
        });
    } else {
        $('#submitPending').hide();
        $('#submitError').show();
    }
}

function clearInput() {
    'use strict';
    $.each($('.plankton-box'), function (index, box) {
        var input = $(box);
        if (input.prop('tagName') === 'BUTTON') {
            input.prop('value', '0');
            input.text('0');
            input.removeClass('threshold-ok');
            input.removeClass('threshold-warn');
            input.removeClass('threshold-danger');
        } else {
            input.val('');
            removeValid(input);
        }
    });
    $.each($('input[type=radio]'), function (idx, radio) {
        radio.checked = radio.defaultChecked;
    });
}

var validators = {
    'non-empty': function (element) {
        'use strict';
        var val = element.val(),
            name = element.attr('id').split('_').join(' ');
        if (val === null || val.length === 0) {
            return '<p>' + name + ' is empty.</p>';
        }
        return null;
    },
    'number': function (element) {
        'use strict';
        var val = element.val(),
            name = element.attr('id').split('_').join(' ');
        if (val.length > 0 && (isNaN(parseFloat(val)) || isNaN(val - 0))) {
            return '<p>' + name + ' is not a valid number.</p>';
        }
        return null;
    },
    'non-negative': function (element) {
        'use strict';
        var val = element.val(),
            name = element.attr('id').split('_').join(' ');
        if (val.length > 0 && parseFloat(val) < 0) {
            return '<p>' + name + ' is a negative number.</p>';
        }
        return null;
    },
    'default': function (inputElement) {
        'use strict';
        return null;
    }
};
