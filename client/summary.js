var summaryMap,
    summaryMapMarkers = {};

function makeDetailSummary() {
    var siteNames = Object.keys(sites).sort();
    for (var i = 0; i < siteNames.length; i++) {
        var site = siteNames[i],
            brokers = ['GCTide', 'PlanktonPortal', 'Share'];
        if (sites[site].sensors !== undefined) {
            brokers.splice(0, 0, sites[site].sensors);
        }
        var marker = L.marker([sites[site].lat, sites[site].long]).addTo(summaryMap);
        marker.bindPopup('');
        summaryMapMarkers[site] = marker;
        var brokerDetail = $('<div>').attr('class', 'broker-summary').attr('id', site.replace(' ', '_') + '-broker-summary')
                                                                                .append($('<h5>').append(site));
        var accordion = $('#summary-detail').append($('<div>').attr('class', 'summary-accordion').attr('id', site.replace(' ', '_') + '-summary-accordion')
                                        .append($('<div>').attr('class', 'summary-detail-site-name').append(site))
                                        .append($('<div>').attr('class', 'summary-detail-last-upload').attr('id', site.replace(' ', '_') + '-last-upload'))
                                        .click(function () { showDashboard($(this).children()[0].innerHTML, 'Detailed Dashboard') })
                                        .hover(function () {
                                            var site = $(this).children()[0].innerHTML;
                                            var content = $(this).next()[0].innerHTML;
                                            summaryMapMarkers[site].setPopupContent(content);
                                            summaryMapMarkers[site].openPopup();
                                        }, function () {
                                            var site = $(this).children()[0].innerHTML;
                                            summaryMapMarkers[site].closePopup();
                                        }));

        accordion.append(brokerDetail);
        for (var j = 0; j < brokers.length; j++) {
            var broker = brokers[j];
            brokerDetail.append($('<p>').attr('id', site.replace(' ', '_') + '-' + broker + '-last-upload'));
        }
        loadBrokerStatus(site, brokers);
    }
    if (siteNames.length === 1) {
        $('.summary-accordion').hide();
        $('.broker-summary').show();
    }
}

function loadBrokerStatus(site, brokers) {
    getData({'bname': site + '_config', 'qids': brokers.join(','), 'limit': 1}, null, null,function (resp) {
        var lastUpload = moment(resp[resp.length - 1].ts);
        lastUpload = Math.floor(moment.duration(moment(new Date()).diff(lastUpload)).asHours());
        if (lastUpload < 12) {
            $('#' + site.replace(' ', '_') + '-summary-accordion').addClass('up');
        } else {
            $('#' + site.replace(' ', '_') + '-summary-accordion').addClass('offline');
        }
        $.each(resp, function (idx, brokerStatus) {
            $('#'+ site.replace(' ', '_') + '-' + brokerStatus.qid + '-last-upload').text(brokerStatus.qid + ': ' + moment(brokerStatus.ts).format('YYYY/MM/DD HH:mm:ss'));
        });
        $('#' + site.replace(' ', '_') + '-last-upload').text(lastUpload + ' hrs');
    });
    setInterval(function () {
        getData({'bname': site + '_config', 'qids': brokers.join(','), 'limit': 1}, null, null,function (resp) {
            var lastUpload = moment(resp[resp.length - 1].ts);
            lastUpload = Math.floor(moment.duration(moment(new Date()).diff(lastUpload)).asHours());
            if (lastUpload < 12) {
                $('#' + site.replace(' ', '_') + '-summary-accordion').addClass('up');
            } else {
                $('#' + site.replace(' ', '_') + '-summary-accordion').addClass('offline');
            }
            $.each(resp, function (idx, brokerStatus) {
                $('#'+ site.replace(' ', '_') + '-' + brokerStatus.qid + '-last-upload').text(brokerStatus.qid + ': ' + moment(brokerStatus.ts).format('YYYY/MM/DD HH:mm:ss'));
            });
            $('#' + site.replace(' ', '_') + '-last-upload').text(lastUpload + ' hrs');
        });
    }, 600000)
}

function loadSummaryPage() {
    $('#main').append($('<div>').attr('id', 'summary-dashboard').attr('class', 'container-fluid').attr('style', 'display:none'));
    $('#summary-dashboard').load('summary.html', null, function () {
        summaryMap = L.map('summary-map').setView([49.61607, -125.25896921753883], 8);
        // FIXME: put this shit somewhere else!
        var accessToken = 'pk.eyJ1IjoiYWZyZWl0YXMwOSIsImEiOiJjazFwaWVqbHkweGdrM2NyeXp2dXNwb3d4In0.CWVtvcv0oC7fT8FlIXNOyA';
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + accessToken, {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 15,
            id: 'mapbox.streets',
            accessToken: accessToken
        }).addTo(summaryMap);
        makeDetailSummary();
    });
}

function showSummaryPage() {
    $('#main').children().hide();
    $('#summary-dashboard').show();
}
