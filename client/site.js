var sites = [],
    streams = [];


function getSites(includeDashboards, callback) {
    $.get({
        url: url('sites'),
        data: {includeDashboards: includeDashboards},
        contentType: 'application/json',
        dataType: 'json'
    }).done(function (responseData, textStatus, jqXHR) {
        if (callback !== undefined) {
            callback(responseData);
        }
    });
}

function loadDashboard(dbId, dbName, dashboard) {
    var db = $('#' + dbId).find('.grid');
    for (var i = 0; i < dashboard.length; i++) {
        loadChart(dashboard[i], db);
    }
    dashboard.loaded = true;
}

function loadChart(chartDef, dashboardElem) {
    var chartId = 'chart-' + Math.random().toString(36).substr(2, 10);
    dashboardElem.append($('<div>').attr('id', chartId)
                                   .attr('class', 'grid-item')
                                   .attr('style', 'width:' + chartDef.width + ';height:' + chartDef.height));
    var streamIds = [];
    for (var i = 0; i < chartDef.chartConfig.series.length; i++) {
        streamIds.push(chartDef.chartConfig.series[i].streamId);
    }
    setTimeout(function () {
        var chart = Highcharts.chart(chartId, chartDef.chartConfig);
        for (i = 0; i < chart.series.length; i++) {
            $.each(streams, function (idx, stream) {
                if (stream.streamId === streamIds[i]) {
                    chart.series[i].streamId = streamIds[i];
                    stream.series.push(chart.series[i]);
                    if (chart.series[i].type === 'bar' && (stream.data || []).length > 0) {
                        chart.series[i].setData(stream.data.map(function (d) { return {'y': d[1], 'ts': moment(d[0])}; }), false);
                    } else {
                        chart.series[i].setData(stream.data, false);
                        if (stream.name === 'Tide Height') {
                            stream.series[i].xAxis.addPlotLine({
                                id: 'now',
                                value: Date.now(),
                                color: 'red',
                                'width': 1,
                                'zIndex': 1000
                            });
                        }
                    }

                }
            });
            advanceProgressBar();
        }
        chart.redraw();
    }, 0.5);
}

function getChartData() {
    $.each(streams, function (idx, stream) {
        // FIXME: only grab a week's worth of data
        var params = {};
        $.each(Object.keys(stream), function (idx, key) {
            if (key !== 'series' && key !== 'data') {
                params[key] = stream[key];
            }
        });
        getData(params, dt(new Date(0)), dt(new Date(100000000000000)), function (data) {
            stream.data = data.map(function (d) { return [Date.parse(d.ts), d.val]; });
            if (data.length === 0) {
                return;
            }
            if (stream.limit !== 0) {
                stream.data = stream.data.reverse();
            }
            if (stream.name === 'Depth') {
                stream.data.splice(0, 0, [0, 0]);    // so that depth lines up on the trellis
            }
            $.each(stream.series, function (idx, series) {
                if (series.type === 'bar') {
                    series.setData(stream.data.map(function (d) { return d[1]; }), true);
                } else {
                    series.setData(stream.data, true);
                    if (stream.name === 'Tide Height') {
                        series.xAxis.removePlotLine('now');
                        series.xAxis.addPlotLine({
                            id: 'now',
                            value: Date.now(),
                            color: 'red',
                            'width': 1,
                            'zIndex': 1000
                        });
                    }
                }
            });
        });
    });
}

function showDashboard(siteName, dbName) {
    var dbId = (siteName + ' ' + dbName).split(' ').join('_');
    var dashboard = sites[siteName].dashboards[dbName];
    $('#main').children().hide();
    $('#site-dashboards').show();
    $('#site-dashboards').children().hide();
    if (dashboard.loaded === undefined) {
        initProgressBar(dashboard.length);
        $('#site-dashboards').hide();
        $('#mainSpinner').show();
        loadDashboard(dbId, siteName + ' ' + dbName, dashboard);
        setTimeout(function () {
            $('.highcharts-data-table').attr('class', 'highcharts-data-table grid-item')
                                       .children().attr('class', 'table table-striped table-bordered');
            $('.highcharts-data-table').children('table').DataTable({
                lengthChange: false,
                paging: false,
                scrollY: true,
                scrollX: false,
                searching: false,
                info: false,
                order: [0, 'desc']
            }).columns([1]).visible(false);
            $('.highcharts-table-caption').hide();
            $('.highcharts-container').css('overflow', 'visible');
            $('#mainSpinner').hide();
            $('#site-dashboards').show();
        }, 0.5);
    }
    $('#' + dbId).show();
}

function updateNavBarSites() {
    if (Object.keys(sites).length === 1) {
        var siteName = Object.keys(sites)[0];
        $('#navbar-list').append($('<li>').attr('class', 'nav-item')
                                    .append($('<a>').attr('class', 'nav-link').attr('onclick', 'showDashboard("' + siteName + '", "Detailed Dashboard");')
                                                .attr('href', '#').append('Site Dashboard')));
    } else {
        $('#navbar-list').append($('<li>').attr('class', 'nav-item dropdown')
                                    .append($('<a>').attr('class', 'nav-link dropdown-toggle').attr('href', '#').attr('id', 'navbarDropdownMenuLink')
                                                .attr('data-toggle', 'dropdown').attr('aria-haspopup', 'true').attr('aria-expanded', 'false').append('Site Dashboard'))
                                    .append($('<div>').attr('class', 'dropdown-menu').attr('aria-labelledby', 'navbarDropdownMenuLink').attr('id', 'nav-site-list')));
    }
}

function loadSiteDashboards() {
    $('#main').append($('<div>').attr('id', 'site-dashboards').attr('class', 'container-fluid').attr('style', 'display:none'));
    var siteNames = Object.keys(sites).sort();
    updateNavBarSites();
    for (var i = 0; i < siteNames.length; i++) {
        var siteName = siteNames[i];
        var siteDashboards = sites[siteName].dashboards;
        streams = streams.concat(sites[siteName].streams);
        $('#nav-site-list').append($('<div>').attr('class', 'dropdown-item').attr('onclick', 'showDashboard("' + siteName + '", "Detailed Dashboard");').append(siteName));
        $.each(siteDashboards, function(dbName, dashboard) {
            var dbId = (siteName + ' ' + dbName).split(' ').join('_');
            $('#site-dashboards').append($('<div id="' + dbId + '" style="display:none">')
                                        .append($('<h2>').attr('class', 'dashboard-title').append(siteName))
                                        .append($('<div class="grid">')));
        });
    }
    setInterval(getChartData, 300000);
    getChartData();
}