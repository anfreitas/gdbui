import json
import os
from copy import deepcopy
import re

working_dir = os.path.dirname(os.path.realpath(__file__))


def to_float(text):
    try:
        retval = float(text)
    except ValueError:
        retval = text
    return retval


def natural_sort(key):
    if isinstance(key, tuple):
        key = key[0]
    return [to_float(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', key)]


def make_gauges(streams, key, suffix, min, max, tide_chart=None):
    gauges = []
    gauge_label = "<div style='text-align:center'><span style='font-size:25px'>{y}</span><br/><span style='font-size:12px;opacity:0.4'>%s</span></div>"
    for stream in streams:
        if key in stream['name']:
            gauge = deepcopy(simple_gauge)
            gauge['chartConfig']['series'][0]['name'] = stream['name']
            gauge['chartConfig']['series'][0]['streamId'] = stream['streamId']
            gauge['chartConfig']['yAxis']['title']['text'] = stream['name']
            gauge['chartConfig']['series'][0]['tooltip']['valueSuffix'] = suffix
            gauge['chartConfig']['series'][0]['dataLabels']['format'] = gauge_label % suffix
            gauge['chartConfig']['yAxis']['min'] = min
            gauge['chartConfig']['yAxis']['max'] = max
            gauges.append(gauge)
            if tide_chart is not None:
                tide_chart['chartConfig']['series'].append({
                    "data": [],
                    "type": "spline",
                    "name": stream['name'],
                    "streamId": stream['streamId']
                })
    return gauges


def make_trellis_charts(streams):
    trellis = []
    if len(streams) == 4:
        trellis_chart['width'] = 22.5
    for i, stream in enumerate(streams):
        trellis_part = deepcopy(trellis_chart)
        trellis_part['width'] = str(trellis_part['width'] + 10 if i == 0 else trellis_part['width']) + '%'
        trellis_part['chartConfig']['chart']['marginLeft'] = 100 if i == 0 else 10
        trellis_part['chartConfig']['title']['text'] = stream['name']
        trellis_part['chartConfig']['title']['x'] = 90 if i == 0 else 0
        trellis_part['chartConfig']['xAxis']['categories'] = [s.replace(stream['name'], '').strip() for s in stream['names']]
        trellis_part['chartConfig']['xAxis']['labels']['enabled'] = i == 0
        trellis_part['chartConfig']['yAxis']['min'] = [30, 0, 0, 0][i]
        trellis_part['chartConfig']['yAxis']['max'] = [150, 10, 20, 20][i]
        trellis_part['chartConfig']['series'] = [{
            'data': [],
            'streamId': stream['streamId'],
            'dataLabels': [{
                'align': 'left',
                'format': '{y}'
            }]
        }]
        trellis.append(trellis_part)
    return trellis


def make_tide_chart(tide_stream, title, axis_title):
    chart = deepcopy(tide_chart)
    chart['chartConfig']['title']['text'] = title
    chart['chartConfig']['yAxis'][0]['title']['text'] = axis_title
    chart['chartConfig']['series'][0]['streamId'] = tide_stream['streamId']
    chart['width'] = '100%'
    return chart


def make_detailed_dashboard(tide_stream, detailed_streams):
    return [make_tide_chart(tide_stream, 'Tide Height', '')] + make_trellis_charts(detailed_streams) + []


def make_average_dashboard(tide_stream, average_streams):
    combined_averages = make_tide_chart(tide_stream, 'Combined Averages', 'DO %Saturation [%]')
    combined_averages['chartConfig']['yAxis'].append({"title": {"text": "DO [mg/L]"}})
    combined_averages['chartConfig']['yAxis'].append({"title": {"text": "Water Temperature [°C]"}})
    dashboard = [combined_averages,
                 make_gauges(average_streams, 'Saturation', '%Sat', 30, 150, combined_averages)[0],
                 make_gauges(average_streams, 'mg/L', 'mg/L', 0, 10, combined_averages)[0],
                 make_gauges(average_streams, 'Temperature', '°C', 0, 20, combined_averages)[0]]
    combined_averages['chartConfig']['series'][2]['yAxis'] = 2
    combined_averages['chartConfig']['series'][3]['yAxis'] = 3
    combined_averages['chartConfig']['exporting'] = {'showTable': True}
    for i in range(1, 4):
        dashboard[i]['width'] = '333px'
        dashboard[i]['height'] = '300px'
        dashboard[i]['chartConfig']['yAxis']['title']['y'] = -100
    return dashboard


site_configs = os.listdir(os.path.join(working_dir, '..', 'sites', 'grieg'))
with open(os.path.join(working_dir, '..', 'charts', 'tide_chart.json')) as f:
    tide_chart = json.loads(f.read())
with open(os.path.join(working_dir, '..', 'charts', 'simple_gauge.json')) as f:
    simple_gauge = json.loads(f.read())
with open(os.path.join(working_dir, '..', 'charts', 'trellis_part.json')) as f:
    trellis_chart = json.loads(f.read())


for site_config in site_configs:
    with open(os.path.join(working_dir, '..', 'sites', 'grieg', site_config)) as f:
        site_config = json.loads(f.read())

    gid = site_config['gid']
    keys = ['Oxygen Saturation', 'Oxygen mg/L', 'Water Temperature', 'Depth']
    detailed_streams = []
    average_streams = []
    for key in keys:
        qids = [qid for qid, name in sorted(site_config.items(), key=natural_sort) if key in name]
        if len(qids) == 0:
            continue
        bname = '_'.join((gid, qids[0].split('-')[0]))
        detailed_streams.append({
            'bname': bname,
            'qids': ','.join(qids),
            'name': key,
            'streamId': '%s-%s' % (gid, key.lower().replace(' ', '-')),
            'series': [],
            'names': [name for qid, name in sorted(site_config.items(), key=natural_sort) if key in name],
            'limit': 1
        })
        average_streams.append({
            'bname': bname,
            'qids': ','.join(qids),
            'sample_rate': '12H',
            'sample_offset': '1H',
            'agg': 'mean',
            'name': 'Average ' + key,
            'streamId': '%s-%s-%s' % (gid, 'average', key.lower().replace(' ', '-')),
            'series': []
        })

    tide_stream = {
        'bname': '%s_GCTide' % gid,
        'qids': [k for k in site_config.keys() if 'GCTide' in k][0],
        'name': 'Tide Height',
        'streamId': '%s-tide-height' % gid,
        'series': []
    }

    dashboards = {'Detailed Dashboard': make_detailed_dashboard(tide_stream, detailed_streams),
                  'Averages Dashboard': make_average_dashboard(tide_stream, average_streams)}
    streams = detailed_streams + average_streams + [tide_stream]

    out_file = os.path.join(working_dir, '..', 'client', 'sites', gid + '.json')
    with open(out_file, 'w') as f:
        #f.write('var streams = (streams || []).concat(%s);\ndashboards["%s"] = %s;\nsitesAvailable["%s"] = "%s";' %
        #        (json.dumps(streams, indent=2), gid, json.dumps(dashboards, indent=2), gid,
        #         'AKVA' if 'AKVA' in list(site_config.keys())[0] else 'Steinsvik'))
        f.write(json.dumps({'streams': streams, 'dashboards': dashboards}, indent=2))

    print('Site client code written to %s' % out_file)
